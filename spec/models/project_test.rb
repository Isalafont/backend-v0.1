# frozen_string_literal: true

require 'test_helper'

class ProjectTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  describe 'factory' do
    it 'is valid' do
      expect(build(:project)).to be_valid
    end
  end

  describe 'feed' do
    it 'exists and is created' do
      project = create(:project)
      expect(project.feed).not_to be nil
    end
  end

  describe 'updating activity' do
    before do
      @project = create(:project)
    end

    it 'updates on user add' do
      user = create(:user)
      old_ts = @project.updated_at
      @project.users << user
      expect(old_ts).not_to eq @project.updated_at
    end

    it 'updates on need add' do
      need = create(:need)
      old_ts = @project.updated_at
      @project.needs << need
      expect(old_ts).not_to eq @project.updated_at
    end

    it 'updates on post add' do
      post = create(:post)
      old_ts = @project.updated_at
      @project.feed.post << post
      expect(old_ts).not_to eq @project.updated_at
    end

    it 'updates on need add' do
      need = create(:need)
      old_ts = @project.updated_at
      @project.needs << need
      expect(old_ts).not_to eq @project.updated_at
    end
  end
end
