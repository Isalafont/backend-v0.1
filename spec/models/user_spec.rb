# frozen_string_literal: true

require 'rails_helper'
RSpec.describe User, type: :model do
  before(:each) do
    @user1 = create(:user)
  end

  it 'needs confirmation' do
    expect(@user1.confirmed?).to be false
  end

  it 'is confirmed' do
    @user1.confirm
    expect(@user1.confirmed?).to be true
  end

  it 'is valid with valid attributes' do
    expect(@user1).to be_valid
  end

  it 'has a unique email' do
    user2 = build(:user, email: @user1.email)
    expect(user2).not_to be_valid
  end
  # TODO: must only work on update not create
  # it "has a unique nickname" do
  #   user2 = build(:user, nickname: @user1.nickname)
  #   expect(user2).not_to be_valid
  # end

  describe '#create_feed' do
    it 'triggers create feed on create' do
      user = build(:user)
      expect(user).to receive(:create_feed).and_call_original
      user.save
      expect(user.feed).to be_present
    end
  end

  it 'is not valid without a password' do
    user2 = build(:user, password: nil)
    expect(user2).not_to be_valid
  end

  it 'is not valid without an email' do
    user2 = build(:user, email: nil)
    expect(user2).not_to be_valid
  end

  it 'sets the user status to active upon create' do
    new_user = create(:user)
    expect(new_user).to be_active
  end

  describe '#deleted?' do
    it "is deleted if it's archived" do
      @user1.archived!
      expect(@user1).to be_deleted
    end
  end

  describe '#make_address' do
    it 'combines address, city, country into an address string' do
      user = build(:user,
                   address: '123 Green Street',
                   city: 'Chicago',
                   country: 'U.S.A.')
      expect(user.make_address).to eq('123 Green Street, Chicago, U.S.A.')
    end

    it 'uses the IP if there is no city' do
      user = build(:user, city: nil, ip: '1234')
      expect(user.make_address).to eq('1234')
    end
  end

  describe 'coutry validation' do
    it 'validates country if present' do
      user = build(:user, country: 'USA')
      expect(user).to_not be_valid
      expect(user.errors.full_messages).to include('Country is not included in the list')
    end

    it 'is valid if country is in list' do
      user = build(:user, country: 'United States')
      expect(user).to be_valid
    end

    it 'allows country to be nil' do
      user = build(:user, country: nil)
      expect(user).to be_valid
    end
  end

  describe '#clapped?(object)' do
    it 'is true if the user has clapped the object' do
      post = create(:post)
      @user1.owned_relations.create(resource: post, has_clapped: true)

      expect(@user1.clapped?(post)).to eq(true)
    end

    it 'is false if the user has not clapped the object' do
      post = create(:post)

      expect(@user1.clapped?(post)).to eq(false)
    end
  end

  describe '#saved?(object)' do
    it 'is true if the user has saved the object' do
      post = create(:post)
      @user1.owned_relations.create(resource: post, saved: true)

      expect(@user1.saved?(post)).to eq(true)
    end

    it 'is false if the user has not saved the object' do
      post = create(:post)

      expect(@user1.saved?(post)).to eq(false)
    end
  end

  describe '#follows?(object)' do
    it 'is true if the user has followed the object' do
      post = create(:post)
      @user1.owned_relations.create(resource: post, follows: true)

      expect(@user1.follows?(post)).to eq(true)
    end

    it 'is false if the user has not followed the object' do
      post = create(:post)

      expect(@user1.follows?(post)).to eq(false)
    end
  end

  describe 'logo_url_sm' do
    it 'picks a pseudo-random avatar image' do
      expect(@user1.logo_url_sm).to match(%r{http://localhost:3001/assets/default-user-\d+-\w*.png})
    end

    it 'is always the same' do
      # test to ensure random gets re-seeded each time it's called
      expect(@user1.logo_url_sm).to eq(@user1.logo_url_sm)
    end

    it 'calls default_logo_url if avatar.attachment is not available' do
      expect(@user1).to receive(:default_logo_url).with(no_args)
      @user1.logo_url_sm
    end

    it "uses the avatar image if it's present" do
      @user1.avatar.attach(
        io: File.open(Rails.root.join('spec', 'test-image.png')),
        filename: 'test-image'
      )

      expect(@user1).to receive(:avatar_variant_url).with(resize: '40x40^')
      @user1.logo_url_sm
    end

    it "uses the avatar image if it's present" do
      @user1.avatar.attach(
        io: File.open(Rails.root.join('spec', 'test-image.svg')),
        filename: 'test-image'
      )

      expect(@user1).to receive(:avatar_blob_url)
      @user1.logo_url_sm
    end
  end

  describe '#has_badge?' do
    it 'is true if the user has the badge' do
      badge = Merit::Badge.create!(
        id: 11,
        name: 'banana',
        description: "It's Yellow!"
      )
      @user1.add_badge(badge.id)

      expect(@user1.has_badge?('banana')).to eq(true)
    end

    it 'is false if the user does not have the badge' do
      expect(@user1.has_badge?('banana')).to eq(false)
    end
  end

  describe '#follow_mutual' do
    before do
      @user0 = create(:confirmed_user)
      @user1 = create(:confirmed_user)
      @user2 = create(:confirmed_user)
      @user3 = create(:confirmed_user)
      @other = create(:confirmed_user)
      @user0.owned_relations.create(resource: @user1, follows: true)
      @user0.owned_relations.create(resource: @user2, follows: true)
      @user0.owned_relations.create(resource: @user3, follows: true)
      @other.owned_relations.create(resource: @user1, follows: true)
      @other.owned_relations.create(resource: @user3, follows: true)
    end

    it 'returns the mutual followers between two users' do
      mutual = @user0.follow_mutual(@other)
      expect(mutual.count).to eq(2)
      expect(mutual.pluck(:id)).to include(@user1.id)
      expect(mutual.pluck(:id)).to include(@user3.id)
      expect(mutual.pluck(:id)).not_to include(@user2.id)
    end

    it 'returns the right count of mutual connection' do
      expect(@user0.follow_mutual_count(@other)).to eq(2)
    end
  end

  describe '#projects_count' do
    it 'returns zero if the user is not associated with a project' do
      # Check that the project count is 0
      expect(@user1.projects_count).to eq(0)
    end

    it 'does not include projects where  the user\'s role is pending' do
      # Create a project
      project = create(:project, creator: @user1)
      project.users << @user1
      @user1.add_role(:member, project)

      # Add a pending user
      user_pending = create(:user)
      project.users << user_pending
      user_pending.add_role(:pending, project)

      # We expect the pending user's project count be 0
      expect(user_pending.projects_count).to eq(0)
    end

    it 'returns the count of projects where the user is a member' do
      # Create a project
      project = create(:project, creator: @user1)
      project.users << @user1
      @user1.add_role(:member, project)

      # We expect the pending user's project count be 1
      expect(@user1.projects_count).to eq(1)
    end
  end

  describe '#needs_count' do
    it 'includes needs where the user is a member' do
      create(:need, user: @user1)
      create(:need)
      expect(@user1.needs_count).to eq(1)
    end
  end

  describe '#communities_count' do
    it 'includes communities where the user is a member' do
      create(:community, creator: @user1)
      create(:community)
      expect(@user1.communities_count).to eq(1)
    end
  end

  describe '#age' do
    it 'defaults to using `age` db field if birth date is not present' do
      josh = create(:user, age: 24, first_name: 'Josh')
      expect(josh.age).to eq(24)
    end

    it 'uses birth_date if present' do
      josh = create(:user, age: nil, first_name: 'Josh', birth_date: Date.today)
      Timecop.freeze(Date.today + 23.years + 1.day) do
        expect(josh.age).to eq(23)
      end
    end
  end
end
