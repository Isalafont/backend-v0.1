# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Datasets::CkanDataset, type: :model do
  # pending "add some examples to (or delete) #{__FILE__}"

  describe 'validation' do
    before(:each) do
      @user = create(:confirmed_user)
    end

    it 'should have valid factory' do
      expect(build(:ckan_dataset)).to be_valid
    end

    it 'reject invalid url' do
      d = Dataset.new(author_id: @user.id, type: 'Datasets::CkanDataset', url: 'something')
      expect(d).to be_invalid
    end

    it 'needs an author' do
      d = Dataset.new(author_id: @user.id, type: 'Datasets::CkanDataset', url: 'something')
      expect(d).to be_invalid
    end
  end
end
