# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Project, type: :model do
  describe 'associations' do
    it { should have_many(:challenges) }
    it { should have_many(:users_projects) }
    it { should have_one(:feed) }
    it { should have_and_belong_to_many(:skills) }
    it { should have_and_belong_to_many(:interests) }
  end

  describe '#create_feed' do
    it 'triggers create feed on create' do
      project = build(:project)
      expect(project).to receive(:create_feed).and_call_original
      project.save
      expect(project.feed).to be_present
    end
  end

  describe 'validation' do
    let!(:project) { create(:project, creator_id: create(:user).id) }
    it 'should have valid factory' do
      expect(project).to be_valid
    end

    # it "should have unique short title" do
    #   project2 = build(:project, :short_title => project.short_title)
    #   expect(project2).not_to be_valid
    #   expect(project2.errors[:short_title]).to include("has already been taken")
    # end
  end

  describe 'members' do
    it 'does something' do
      project = create(:project)
      expect(project.members).to include(project.creator)
      expect(project.members.size).to eq(1)
    end
  end
end
