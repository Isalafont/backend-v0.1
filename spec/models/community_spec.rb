# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Community, type: :model do
  describe 'associations' do
    it { should have_and_belong_to_many(:community_tags) }
    it { should have_and_belong_to_many(:skills) }
    it { should have_and_belong_to_many(:interests) }
    it { should have_one(:feed) }
    it { should have_many(:users_communities) }
    it { should have_many(:challenges_communities) }
  end

  describe '#create_feed' do
    it 'triggers create feed on create' do
      @user = create(:user)
      community = build(:community, creator_id: @user.id)
      expect(community).to receive(:create_feed).and_call_original
      community.save
      expect(community.feed).to be_present
    end
  end

  describe 'validation' do
    before(:each) do
      @user = create(:user)
      @community = create(:community, creator_id: @user.id)
    end

    it 'should have valid factory' do
      expect(@community).to be_valid
    end

    it 'should have unique short title' do
      community2 = build(:community, creator_id: @user.id, short_title: @community.short_title)
      expect(community2).not_to be_valid
      expect(community2.errors[:short_title]).to include('has already been taken')
    end
  end

  describe 'includes utils module' do
    context 'methods should be defined inside the module' do
      # Previous to this commit, these three methods were defined OUTSIDE the
      # Utils module, which would mean the methods were defined in "main" in
      # Ruby. In Rails, for reasons I don't know yet, those "main" methods are
      # hoisted and attached BasicObject, as private methods, which raises the
      # error "#<NoMethodError: private method `users_sm' called for X".
      # These are the failing tests that pass when these methods are moved into
      # the Utils module, which also allows us to remove the "public :users_sm"
      # hotfixes from challenge/community/project

      it 'responds to #banner_url_sm' do
        expect(described_class.new).to respond_to(:banner_url_sm)
        expect { described_class.new.banner_url_sm }.to_not raise_error
      end

      it 'responds to #members_count' do
        expect(described_class.new).to respond_to(:members_count)
        expect { described_class.new.members_count }.to_not raise_error
      end

      it 'responds to #users_sm' do
        expect(described_class.new).to respond_to(:users_sm)
        expect { described_class.new.users_sm }.to_not raise_error
      end
    end

    it 'responds to #feed_id' do
      expect(described_class.new).to respond_to(:feed_id)
    end
  end
end
