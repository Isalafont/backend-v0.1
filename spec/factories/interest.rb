# frozen_string_literal: true

require 'ffaker'

FactoryBot.define do
  factory :interest do
    interest_name { FFaker::Movie.title }
  end
end
