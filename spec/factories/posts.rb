# frozen_string_literal: true

FactoryBot.define do
  factory :post do
    # Need to set 2 parameters !
    # feed_id: ID of an existing feed
    # user_id: id of an existing user
    feed
    user
    from { user }
    media { FFaker::Avatar.image }
    content { FFaker::DizzleIpsum.paragraph }
  end
end
