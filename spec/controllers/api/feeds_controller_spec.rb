# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::FeedsController, type: :controller do
  before do
    @user = create(:confirmed_user)
    sign_in @user
    @project = create(:project, creator_id: @user.id)
    @feed = create(:feed, feedable: @project)
  end

  describe '#index' do
    context 'with logged in user' do
      it 'should return list of feed' do
        profile_feed_post = create(:post, user: @user, feed: @user.feed, from: @user)
        @user.feed.posts << profile_feed_post
        followed_program = create(:program)
        @user.owned_relations.create(resource: followed_program, follows: true)
        # @user.owned_relations << @feed
        # TODO: create a post on something the user follows
        followed_program_post = create(:post, from: followed_program)
        followed_program.feed.posts << followed_program_post

        get :index

        json_response = JSON.parse(response.body)
        expect(json_response.count).to eq(2)
        # check the order is by descending created_at timestamp
        expect(json_response[0]['content']).to eq(followed_program_post.content)
        expect(json_response[1]['content']).to eq(profile_feed_post.content)
        expect(response).to have_http_status :ok
      end
    end
    context 'with logged out  user' do
      it 'should not return list of feed' do
        sign_out @user
        get :index
        expect(response).to have_http_status :unauthorized
      end
    end
  end

  describe '#show' do
    it 'should return feed' do
      get :show, params: { id: @feed.id }
      expect(response.status).to eq 200
    end
  end

  describe '#remove_post' do
    before(:each) do
      @user2 = create(:confirmed_user)
      @post = create(:post, user_id: @user2.id)
      @project.feed.posts << @post
    end

    it 'should remove a post from a feed' do
      @user.add_role :admin, @project
      delete :remove_post, params: { id: @project.feed.id, post_id: @post.id }
      expect(response.status).to eq 200
      expect(@project.feed.posts.count).to eq 0
    end

    it 'should be forbidden to not admin' do
      @user.remove_role :admin, @project
      @user.remove_role :owner, @project
      delete :remove_post, params: { id: @project.feed.id, post_id: @post.id }
      expect(response.status).to eq 403
      expect(@project.feed.posts.count).to eq 1
    end
  end
end
