# Changelog

## 14/4/2020
- Adding triggers to RecSys
- Adding specs specs specs !
- Massive rewrite of the code, concerns are now much more self contained !
- BUG: Found a bug on ressources where there was not through table so only one personne could have a ressource at a time ! O.o !
- Corrected the bug and made a migration that should solve the problem and copy the data.

## 13/4/2020
- Added the Badge system to the backend using the Merit Gem
- Working on the RecSys system

## 8/4/2020
- Adding the dataset / data / datafield Models
- Adding the dataset controller
- Adding the License Model
- Adding the Source Model
- Adding Dataset types from inheritance of Dataset CkanDataset KapCodeDataset CustomDataset
- Adding the Tag Model
- Adding Paper trail gem with versioning
`bundle exec rails generate paper_trail:install --with-changes`

To add as source:
Datahub (Looks similar to CKAN ! is it a CKAN ???)
https://datahub.io/core/geo-countries/datapackage.json

https://duraspace.org/dspace/resources/hosted-dspace/

DSpace, Dataverse, Zenodo

CKAN ressource views

https://data.gov.sg/api/action/resource_view_list?id=f9dbfc75-a2dc-42af-9f50-425e4107ae84
