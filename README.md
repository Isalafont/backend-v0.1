# README

## Badges

### Skylight

[![View performance data on Skylight](https://badges.skylight.io/problem/nv7aSx9ZHCsi.svg)](https://oss.skylight.io/app/applications/nv7aSx9ZHCsi)
[![View performance data on Skylight](https://badges.skylight.io/typical/nv7aSx9ZHCsi.svg)](https://oss.skylight.io/app/applications/nv7aSx9ZHCsi)
[![View performance data on Skylight](https://badges.skylight.io/rpm/nv7aSx9ZHCsi.svg)](https://oss.skylight.io/app/applications/nv7aSx9ZHCsi)
[![View performance data on Skylight](https://badges.skylight.io/status/nv7aSx9ZHCsi.svg)](https://oss.skylight.io/app/applications/nv7aSx9ZHCsi)

## Development environment

Use the docker-compose.yaml in docker-dev to setup the databases for a dev environment
`cd docker-dev && docker-compose up -d`

## Build

To build the docker images use the docker-compose.yaml

`docker-compose build`

This will build two version of the app, one that launches the rails server and one that launches the sidekiq worker

This allows us to build two version and upload from the same exact image to heroku as a web app with workers on the side

## ENV variables needed

### Required

- SMTP_DOMAIN: Domain name is needed
- SMTP_PORT: SMTP Port
- SMTP_URL: SMTP UrL
- SMTP_USER: SMTP User
- SMTP_PASSWORD: SMTP server password
- DATABASE_URL: HEROKU postgress database URL
- SECRET_KEY_BASE: Secret key base hash
- RAILS_ENV: production
- ALGOLIA_API_TOKEN: Algolia API token for frontend search indexing
- ALGOLIA_APP_ID: Algolia App ID for frontend search
- AWS_S3_KEY_ID: Amazon S3 Key ID for file storage (through Active Storage)
- AWS_S3_SECRET: Amazon S3 Secret token for file storage (through Active Storage)
- AWS_S3_REGION: Amazon S3 Region for file storage (through Active Storage)
- AWS_S3_BUCKET: Amazon S3 Bucket name for file storage (through Active Storage)
- BACKEND_URL: URL of the deployment so the API is self aware of its location
- FRONTEND_URL: URL of the frontend deployment so the API is aware of where it must redirect
- DATABASE_URL: Postgress DB URL of the format: postgres://USER:Password@URI:Port/DB_NAME
- PORT: Deployment port useful for any K8S or Heroku style deployment requiring dynamic port mapping
- JOGL_EMAIL: Email address used to send email - FROM field
- RAILS_ENV: `development` or `production`
- REDIS_URL: REDIS URL for sidekiq async job management.
- SECRET_KEY_BASE: Large random number in Hex for encryption seeding

### Optional

- ELASTIC_APM_SERVER_URL: APM server If monitoring with Elastic APM
- ELASTIC_APM_SECRET_TOKEN: APM Token If monitoring with Elastic APM
- ELASTIC_ENV: If you want to name your deployement in the APM
- ELASTICSEARCH_HOST: Set if you want to send through logstash the logs from the backend to an elastic search server
- ELASTICSEARCH_USER: Set if you want to send through logstash the logs from the backend to an elastic search server
- ELASTICSEARCH_PASSWORD: Set if you want to send through logstash the logs from the backend to an elastic search server
- RUBYOPT: '-W0' If you get tired of 2.7 warnings ...
- RAILS_LOG_TO_STDOUT: if the logs should output to the heroku LOG (Can be a security issue!)

## Useful commands for rails

From the JOGL/ repository (**NOT FROM JOGL/Jogl-Backend/**):
_Create databases (if it's first launch)_

```bash
docker-compose run backend rails db:create
```

_Migrate database_

```bash
docker-compose run backend rails db:migrate
```

\*There are only interests seeds currently **YOUR FRONT MAY NOT WORK IF NOT SEEDED\***

```bash
docker-compose run backend rails db:seed:interests
```

_Show all routes_

```bash
docker-compose run backend rails routes
```

_Open rails console_

```bash
docker-compose run backend rails c
```

## Controller architecture

Currently all api controllers are located in [app/controllers/api](https://gitlab.com/JOGL/backend-v0.1/tree/master/app/controllers/api) file.
The API uses custom [active model serializers](https://github.com/rails-api/active_model_serializers) for json rendering. There are few [concers/helpers](https://medium.freecodecamp.org/add-callbacks-to-a-concern-in-ruby-on-rails-ef1a8d26e7ab)
that are created.
Concerns are also used for [serializers](https://gitlab.com/JOGL/backend-v0.1/tree/master/app/serializers/concerns/api) which helps to centralize certain methods
that are used throughout different serializers.
The mailer passes through sidekiq to facilitate the email sending.

## Notes on roles

users can have 3 roles on objects:

- member
- admin
- owner

Those roles are then used to check accross the app for permissions on actions, such as update etc.

some special roles are used for challenges and programs:

- challenge_creator
- program_creator

Those two roles need to be added to an admin user

```
user = User.find(:id)
user.add_role :challenge_creator
user.add_role :program_creator
```

## Database architecture

The usage of models is relatively simple in JOGL*BACKEND for one exception.
We used for several occasions [HABTM](https://guides.rubyonrails.org/association_basics.html#choosing-between-has-many-through-and-has-and-belongs-to-many) (Has And Belong To Many) relations between
different models. The reason why we user [HABTM](https://guides.rubyonrails.org/association_basics.html#choosing-between-has-many-through-and-has-and-belongs-to-many) it's because we had several models
linked together with many to many relation but the difference between [HABTM](https://guides.rubyonrails.org/association_basics.html#choosing-between-has-many-through-and-has-and-belongs-to-many) and
has_many is that you use [HABTM](https://guides.rubyonrails.org/association_basics.html#choosing-between-has-many-through-and-has-and-belongs-to-many) only if you need the ids of each model inside the relational table between 2 models and nothing else. If you have something other
than ids inside the relational (ex: status, role etc..) you'll need to have a
many to many relation (has_many to has_many).
The best example would be User and Interest relation.
\_User has many interests, interests can belong to many users.* Between these 2
models there's a many to many relation meaning that we only need to link User ids
to Interest ids. This way there's no need to create the relational model UserInterests
separately because _has_and_belongs_to_many_ does the job.

# Contribute

JOGL is **100% open source**, and we fully welcome **contribution to the code**!

For the backend, we use the following technologies: Algolia, AmazonS3, Ruby, GraphQL, Elastic Search... If you have experience in one or multiple of them, your help will be much appreciated!

- API first, head to our [API documentation](https://documenter.getpostman.com/view/8688524/SWE84xMg?version=latest) for a current view of the API.
- If you want to add code, fork from the branch `develop` and merge request to the branch `develop`! Any request to master or staging will be automatically rejected.

Feel free to browse through the issues and see if you can help us in one or multiple of them:

- **List view** - View issues in list: [backend-only issues](https://gitlab.com/JOGL/JOGL/-/issues?label_name[]=backend) or [all issues](https://gitlab.com/JOGL/JOGL/-/issues).
- **Board view** - View a Trello-like view of the issues listed by priority and state (to do, doing, ready/review): [backend-only issues](https://gitlab.com/JOGL/JOGL/-/boards/1990878?&label_name[]=backend) or [all issues](https://gitlab.com/JOGL/JOGL/-/boards/885598).

For more detailed information on contributing, please follow [this link](https://gitlab.com/JOGL/JOGL/-/blob/master/CONTRIBUTING.md).
