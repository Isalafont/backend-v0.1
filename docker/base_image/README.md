# Information

This is the base image docker file so that the CI builds are much much faster. For more information:

- https://docs.gitlab.com/ee/user/packages/container_registry/
- https://blog.sparksuite.com/7-ways-to-speed-up-gitlab-ci-cd-times-29f60aab69f9

# Updating

If new gems or new packages must be added then one must follow those steps:

## Build the docker image

From the root directory of the backend, build the image:

`docker-compose build base`

## Tagging the image

We then need to give the image the right tag before pusing it to the GitLab registry

`docker tag base registry.gitlab.com/jogl/backend-v0.1/base:latest`

## Loggin'in the docker registry

We need to login to be able to push !

`docker login registry.gitlab.com`

WARNING: If you have 2FA activated you will need to use a Personal Access Token to login ! (More info here: https://docs.gitlab.com/ee/user/packages/container_registry/#authenticating-to-the-gitlab-container-registry)

## Pushing in the GitLab registry

Then we push the image to the registry

`docker push registry.gitlab.com/jogl/backend-v0.1/base:latest`

# TODO
Add an onchange GEMFILE in the gitlabCI trigger to rebuild the base image when someone adds a GEM so that it is not a manual process anymore. And will avoid colision issues as well.
