# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.7.1'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.2.4.3'
# Use postgresql as the database for Active Record
gem 'pg', '>= 0.18', '< 2.0'
# Use Puma as the app server
gem 'puma'
# Use Redis adapter to run Action Cable in production
gem 'redis'
# Use ActiveModel has_secure_password
gem 'bcrypt'

gem 'devise_token_auth'

gem 'devise'

gem 'devise-async'

gem 'rolify', github: 'RolifyCommunity/rolify', ref: '03dcfd251a37149723a712f6760df053219525d8'

gem 'multipart-post'

gem 'dotenv-rails', groups: %i[development test]

gem 'bullet'

# Required for the API to serve assets such as images
gem 'sass-rails'
gem 'uglifier'

gem 'active_model_serializers'
gem 'panko_serializer'
# Use ActiveStorage variant
# gem 'mini_magick', '~> 4.8'

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.1.0', require: false

# Use Rack CORS for handling Cross-Origin Resource Sharing (CORS), making cross-origin AJAX possible
gem 'rack-cors', require: 'rack/cors'

# Slack
gem 'slack-notifier'

# Rails metrics by Skylight.io
gem 'skylight'

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]

# Algolia search engine
gem 'algoliasearch-rails'

gem 'seedbank'

gem 'sidekiq'
gem 'sidekiq-cron'

gem 'mini_magick'

gem 'aws-sdk-s3', require: false

gem 'gibbon'

gem 'et-orbi'

gem 'pagy'

gem 'merit'

gem 'paper_trail'

gem 'factory_bot_rails'

gem 'notification-pusher-actionmailer'
gem 'notifications-rails'

# Adding Swagger and rspec
group :development, :test do
  gem 'database_cleaner'
  gem 'ffaker'
  gem 'rails-controller-testing'
  gem 'rspec-rails', '~> 3.5'
  gem 'rspec-rails-swagger'
  gem 'rspec-sidekiq'
  gem 'shoulda'
  gem 'simplecov'
  gem 'therubyracer'
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
end

group :test do
  gem 'timecop'
end

# Adding GeoCoding capacity
gem 'geocoder', '>= 1.6.3'

# Adding ELK logstash system
gem 'logstash-logger'
gem 'logstasher'

# Adding ELK APM metrics
gem 'elastic-apm'

gem 'graphql'

# sentry.io application error monitoring
gem 'sentry-raven'
