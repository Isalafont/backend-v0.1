# frozen_string_literal: true

class CreateNetworks < ActiveRecord::Migration[5.2]
  def change
    create_table :networks, force: :cascade do |t|
      t.jsonb :json_network
      t.datetime :created_at, null: false
      t.datetime :updated_at, null: false
    end
  end
end
