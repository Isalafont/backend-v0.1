# frozen_string_literal: true

class CreateDatafields < ActiveRecord::Migration[5.2]
  def change
    create_table :datafields do |t|
      t.string :name
      t.string :title
      t.string :description
      t.string :format
      t.string :unit
      t.bigint :datum_id
      t.timestamps
    end
  end
end
