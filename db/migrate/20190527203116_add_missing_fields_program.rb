# frozen_string_literal: true

class AddMissingFieldsProgram < ActiveRecord::Migration[5.2]
  def change
    create_table :users_programs do |t|
      t.belongs_to :user, index: true
      t.belongs_to :program, index: true
      t.string :role
      t.string :part
    end

    add_column :programs, :status, :string
  end
end
