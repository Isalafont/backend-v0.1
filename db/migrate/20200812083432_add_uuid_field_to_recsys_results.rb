class AddUuidFieldToRecsysResults < ActiveRecord::Migration[5.2]
  def change
    add_column :recsys_results, :uuid, :string, default: "uuid_generate_v4()"
    add_index :recsys_results, :uuid
    RecsysResult.update_all(uuid: "uuid_generate_v4()")
  end
end
