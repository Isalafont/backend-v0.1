# frozen_string_literal: true

class AddDefaultStatusValueToChallenges < ActiveRecord::Migration[5.2]
  def change
    reversible do |dir|
      dir.up do
        Program.connection.execute('UPDATE challenges SET status = 0 WHERE status IS NULL')
      end
      dir.down do |_dir|
        Program.connection.execute('UPDATE challenges SET status = NULL WHERE status IS 0')
      end
    end

    change_column_null :challenges, :status, false
    change_column_default :challenges, :status, from: nil, to: 0
  end
end
