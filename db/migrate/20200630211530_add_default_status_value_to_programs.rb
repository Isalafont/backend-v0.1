# frozen_string_literal: true

class AddDefaultStatusValueToPrograms < ActiveRecord::Migration[5.2]
  def change
    reversible do |dir|
      dir.up do
        Program.connection.execute('UPDATE programs SET status = 0 WHERE status IS NULL')
      end
      dir.down do |_dir|
        Program.connection.execute('UPDATE programs SET status = NULL WHERE status IS 0')
      end
    end

    change_column_null :programs, :status, false
    change_column_default :programs, :status, from: nil, to: 0
  end
end
