# frozen_string_literal: true

class ClassifyMention < ActiveRecord::Migration[5.2]
  def up
    Mention.all.each do |mention|
      mention.obj_type = mention.obj_type.capitalize
      mention.save
    end
  end

  def down
    Mention.all.each do |mention|
      mention.obj_type = mention.obj_type.downcase
      mention.save
    end
  end
end
