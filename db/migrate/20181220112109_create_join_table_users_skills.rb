# frozen_string_literal: true

class CreateJoinTableUsersSkills < ActiveRecord::Migration[5.2]
  def change
    create_table :skills_users, id: false do |t|
      t.integer :user_id
      t.integer :skill_id
    end
  end
end
