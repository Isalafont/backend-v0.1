# frozen_string_literal: true
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
# interests = Interest.create!([
#   { interest_name: "No poverty" },
#   { interest_name: "Zero hunger" },
#   { interest_name: "Good health and well-being" },
#   { interest_name: "Quality eductation" },
#   { interest_name: "Gender equality" },
#   { interest_name: "Clean water and sanitation" },
#   { interest_name: "Affordable and clean energy" },
#   { interest_name: "Decent work and economic growth" },
#   { interest_name: "Industry, innovation and infrastructure" },
#   { interest_name: "Reduced inequalities" },
#   { interest_name: "Sustainable cities and communities" },
#   { interest_name: "Responsible consumption and production" },
#   { interest_name: "Climate action" },
#   { interest_name: "Life below water" },
#   { interest_name: "Life and land" },
#   { interest_name: "Peace, justice and strong institutions" },
#   { interest_name: "Partnerships for the goals" }
#   ])
