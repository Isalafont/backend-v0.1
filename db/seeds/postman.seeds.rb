# frozen_string_literal: true

User.create({ email: 'postman@test.com',
              password: 'password',
              password_confirmation: 'password',
              first_name: 'Postman',
              last_name: 'User',
              nickname: 'postmanuser',
              confirmed_at: DateTime.now })
