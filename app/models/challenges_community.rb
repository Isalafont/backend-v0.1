# frozen_string_literal: true

class ChallengesCommunity < ApplicationRecord
  belongs_to :challenge
  belongs_to :community
end
