# frozen_string_literal: true

class Comment < ApplicationRecord
  resourcify
  notification_object
  include RelationHelpers
  include Utils
  include RecsysHelpers

  belongs_to :post
  belongs_to :user

  before_create :sanitize_content
  before_update :sanitize_content

  after_create :notif_new_comment

  validates :content, presence: true

  def frontend_link
    if post
      "/post/#{post.id}"
    else
      # quick fix for orphaned comment breaking notifications
      # longer-term fix: don't orphan comments
      '/404'
    end
  end

  def notif_new_comment
    Notification.for_group(
      :post_participants_except_commenter,
      args: [post, self],
      attrs: {
        category: :comment,
        type: 'new_comment',
        object: self,
        metadata: {
          post_id: post.id,
          author_id: user.id
        }
      }
    )
  end
end
