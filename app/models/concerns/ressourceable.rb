# frozen_string_literal: true

module Ressourceable
  extend ActiveSupport::Concern

  included do
    has_many :ressourceship, as: :ressourceable
    has_many :ressources, through: :ressourceship
  end
end
