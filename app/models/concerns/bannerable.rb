# frozen_string_literal: true

module Bannerable
  extend ActiveSupport::Concern

  included do
    has_one_attached :banner
    validates :banner, content_type: ['image/png', 'image/jpeg', 'image/gif']
  end

  def banner_url(resize = '400x400^')
    return banner_variant_url(resize: resize) if banner.attached? && banner.variable?
    return banner_blob_url if banner.attached? && !banner.variable?

    default_banner_url
  end

  def banner_url_sm
    banner_url('100x100^')
  end

  private

  def default_banner_url
    # NOTE: the below will add an asset digest, so
    # URLS may break if assets are changed in the future
    ActionController::Base.helpers.image_url("default-object.jpg")
  end

  def banner_variant_url(resize:)
    Rails
      .application
      .routes
      .url_helpers
      .rails_representation_url(banner.variant(resize: resize))
  end

  def banner_blob_url
    Rails
      .application
      .routes
      .url_helpers
      .rails_blob_url(banner)
  end
end
