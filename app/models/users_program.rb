# frozen_string_literal: true

class UsersProgram < ApplicationRecord
  belongs_to :user
  belongs_to :program
end
