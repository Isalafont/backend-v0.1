# frozen_string_literal: true

class Relationship < ApplicationRecord
  # This model serves as a self calling table in order to create a follower,
  # following logic on user.
  belongs_to :follower, class_name: 'User'
  belongs_to :followed, class_name: 'User'
  validates :follower_id, presence: true
  validates :followed_id, presence: true
end
