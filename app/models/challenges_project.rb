# frozen_string_literal: true

class ChallengesProject < ApplicationRecord
  belongs_to :challenge
  belongs_to :project

  scope :accepted_projects, -> { where(project_status: 'accepted') }
  scope :accepted_count, -> { accepted_projects.count }

  enum project_status: %i[pending accepted]
end
