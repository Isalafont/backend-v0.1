# frozen_string_literal: true

class ResourceNotFound < ApiExceptions::BaseException
  def initialize(message: 'Resource not found', status: :not_found)
    @data = message
    @status = status
  end
end
