# frozen_string_literal: true

require 'csv'

class NetworkCsvMakerWorker
  include Sidekiq::Worker

  def make_node_id(node)
    node.class.name + '_' + node.id.to_s
  end

  def make_node(node, label, status, created_at, updated_at)
    [make_node_id(node), node.class.name, label, status, created_at, updated_at]
  end

  def make_edge(source, target, edge)
    [make_node_id(source), source.class.name, make_node_id(target), target.class.name, edge.relation_type, edge.value, edge.created_at, edge.updated_at]
  end

  def perform
    # Defining the attributes
    node_attributes = %w{node type label status created_at updated_at}
    edge_attributes = %w{source source_type target target_type relation_type value created_at updated_at}

    # Going over all the nodes
    CSV.open("./tmp/network_nodes.csv", "wb", headers: true) do |csv|
      csv << node_attributes

      Skill.find_each do |skill|
        csv << make_node(skill, skill.skill_name, "", "", "")
      end

      Ressource.find_each do |ressource|
        csv << make_node(ressource, ressource.ressource_name, "", "", "")
      end

      Interest.find_each do |interest|
        csv << make_node(interest, interest.interest_name, "", "", "")
      end

      User.find_each do |user|
        csv << make_node(user, user.nickname, user.active_status, user.created_at, user.updated_at)
      end

      Post.find_each do |post|
        csv << make_node(post, post.id, "", post.created_at, post.updated_at)
      end

      Comment.find_each do |comment|
        csv << make_node(comment, comment.id, "", comment.created_at, comment.updated_at)
      end

      Need.find_each do |need|
        csv << make_node(need, need.id, need.status, need.created_at, need.updated_at)
      end

      Project.find_each do |project|
        csv << make_node(project, project.short_title, project.status, project.created_at, project.updated_at)
      end

      Community.find_each do |community|
        csv << make_node(community, community.short_title, community.status, community.created_at, community.updated_at)
      end

      Challenge.find_each do |challenge|
        csv << make_node(challenge, challenge.short_title, challenge.status, challenge.created_at, challenge.updated_at)
      end

      Program.find_each do |program|
        csv << make_node(program, program.short_title, program.status, program.created_at, program.updated_at)
      end
    end

    # Going over the edges
    CSV.open("./tmp/network_edges.csv", "wb", headers: true) do |csv|
      csv << edge_attributes

      RecsysDatum.find_each do |edge|
        source = edge.sourceable_node
        target = edge.targetable_node
        next if source.nil? || target.nil?

        csv << make_edge(source, target, edge)
      end
    end


    network = Network.new()
    network.node_list.attach(io: File.open('./tmp/network_nodes.csv'), filename: 'network_nodes.csv')
    network.edge_list.attach(io: File.open('./tmp/network_edges.csv'), filename: 'network_edges.csv')
    network.save
  end
end
