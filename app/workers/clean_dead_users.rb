# frozen_string_literal: true

class CleanDeadUsers
  include Sidekiq::Worker

  def perform
    User.all.map do |user|
      next if user.confirmed?

      dt = (Time.now - user.created_at) / 1.hours
      # if user has been created more than 30 days ago, delete it
      user.destroy if dt > 720
    end
  end
end
