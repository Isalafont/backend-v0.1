# frozen_string_literal: true

class PrivateMailer < ApplicationMailer
  def send_private_email(from, to, object, content)
    @from = from
    @object = object
    @to = to
    @content = content
    mail(to: "#{@to.first_name} #{@to.last_name} <#{@to.email}>",
         from: "#{@from.first_name} #{@from.last_name} via JOGL <noreply@jogl.io>",
         reply_to: "#{@from.first_name} #{@from.last_name} <#{@from.email}>",
         subject: "#{@from.first_name} sent you a message")
  end
end
