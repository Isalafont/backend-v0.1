# frozen_string_literal: true

class Api::ChallengeNeedSerializer < ActiveModel::Serializer
  include Api::Relationsserializerhelper
  include Api::Rolesserializerhelper
  include Api::Usersserializerhelper
  include Api::Utilsserializerhelper

  attributes :needs

  # Only needed params for needs of a program are returned
  def needs
    result = []
    done = []
    object.challenges_projects.accepted_projects.map(&:project).compact.map do |project|
      next if done.include? project.id

      project.needs.map do |need|
        result << {
          id: need.id,
          title: need.title,
          content: need.content,
          creator: creator(need),
          documents: need.documents,
          status: need.status,
          feed_id: need.feed.id,
          users_sm: need.users_sm,
          skills: skills(need),
          ressources: ressources(need),
          project: need_project(need),
          is_owner: is_owner(need),
          is_member: is_member(need),
          has_followed: has_followed(need),
          follower_count: need.follower_count,
          members_count: need.members_count,
          posts_count: need.posts_count,
          claps_count: need.claps_count,
          has_clapped: has_clapped(need),
          has_saved: has_saved(need),
          is_urgent: need.is_urgent,
          end_date: need.end_date,
          created_at: need.created_at,
          updated_at: need.updated_at
        }
      end
      done << project.id
    end
    result
  end

  def need_project(need)
    @project = need.project
    if @project.nil?
      {
        id: -1,
        title: need.title
      }
    else
      {
        id: @project.id,
        title: @project.title
      }
    end
  end
end
