class Api::ExternalLinkSerializer < ActiveModel::Serializer
  attributes :id,
             :url,
             :name,
             :icon_url
end
