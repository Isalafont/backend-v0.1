# frozen_string_literal: true

class Api::DocumentSerializer < ActiveModel::Serializer
  attributes :id,
             :title,
             :content

  has_many :users, :serializer => Api::AuthorSerializer
end
