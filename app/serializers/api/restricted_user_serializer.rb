# frozen_string_literal: true

class Api::RestrictedUserSerializer < ActiveModel::Serializer
  include Api::Relationsserializerhelper
  include Api::Rolesserializerhelper
  include Api::Utilsserializerhelper

  attributes :active_status,
             :affiliation,
             # age field ommitted on purpose
             :badges,
             :bio,
             :can_contact,
             :category,
             :city,
             :claps_count,
             :confirmed_at,
             :country,
             :current_sign_in_at,
             :email_notifications_enabled,
             :feed_id,
             :first_name,
             :follower_count,
             :following_count,
             :id,
             :interests,
             :last_name,
             :logo_url_sm,
             :logo_url,
             :mail_newsletter,
             :mail_weekly,
             :needs_count,
             :nickname,
             :projects_count,
             :ressources,
             :saves_count,
             :short_bio,
             :skills,
             :status

  attribute :geoloc, unless: :scope?
  attribute :has_clapped, unless: :scope?
  attribute :has_followed, unless: :scope?
  attribute :has_saved, unless: :scope?
  attribute :is_admin, unless: :scope?

  def scope?
    defined?(current_user).nil?
  end
end
