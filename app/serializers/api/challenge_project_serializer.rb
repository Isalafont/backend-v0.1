# frozen_string_literal: true

class Api::ChallengeProjectSerializer < ActiveModel::Serializer
  include Api::Relationsserializerhelper
  include Api::Rolesserializerhelper
  include Api::Usersserializerhelper
  include Api::Utilsserializerhelper

  attributes :projects

  # Only needed params for projects of a challenge are returned
  def projects
    object.projects.where.not(status: 1).map do |project|
      @relation = ChallengesProject.find_by(challenge_id: object.id, project_id: project.id)
      {
        id: project.id,
        title: project.title,
        short_title: project.short_title,
        banner_url: project.banner_url,
        banner_url_sm: project.banner_url_sm,
        short_description: project.short_description,
        status: project.status,
        creator: creator(project),
        challenge_id: object.id,
        challenge_status: @relation.project_status,
        skills: skills(project),
        interests: interests(project),
        geoloc: geoloc(project),
        is_private: project.is_private,
        is_owner: is_owner(project),
        is_admin: is_admin(project),
        is_member: is_member(project),
        users_sm: project.users_sm,
        has_clapped: has_clapped(project),
        has_followed: has_followed(project),
        has_saved: has_saved(project),
        claps_count: project.claps_count,
        follower_count: project.follower_count,
        needs_count: project.needs_count,
        members_count: members_count(project)
      }
    end
  end
end
