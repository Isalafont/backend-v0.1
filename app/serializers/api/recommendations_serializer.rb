# frozen_string_literal: true

class Api::RecommendationsSerializer < ActiveModel::Serializer
  belongs_to :targetable_node, polymorphic: true
end
