# frozen_string_literal: true

class Api::FollowersSerializer < ActiveModel::Serializer
  include Api::Relationsserializerhelper
  attributes :followers

  def followers
    object.followers.map do |follower|
      follower = FactoryBot.build(:deleted_user) if follower.nil? || follower.deleted?
      data = {
        id: follower.id,
        first_name: follower.first_name,
        last_name: follower.last_name,
        nickname: follower.nickname,
        logo_url: follower.logo_url,
        logo_url_sm: follower.logo_url_sm,
        has_followed: has_followed(follower),
        has_clapped: has_clapped(follower),
        can_contact: follower.can_contact,
        short_bio: follower.short_bio
      }
      if object.class.name == 'User'
        data = data.merge({
                            projects_count: follower.projects_count
                          })
      end
      data
    end
  end
end
