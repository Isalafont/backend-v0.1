# frozen_string_literal: true

class Api::ProgramChallengeSerializer < ActiveModel::Serializer
  include Api::Relationsserializerhelper
  include Api::Rolesserializerhelper
  include Api::Usersserializerhelper
  include Api::Utilsserializerhelper

  attributes :challenges

  # Only needed params for challenges of a challenge are returned
  def challenges
    object.challenges.map do |challenge|
      {
        id: challenge.id,
        title: challenge.title,
        title_fr: challenge.title_fr,
        short_title: challenge.short_title,
        short_description: challenge.short_description,
        short_description_fr: challenge.short_description_fr,
        banner_url: challenge.banner_url,
        banner_url_sm: challenge.banner_url_sm,
        logo_url: challenge.logo_url,
        logo_url_sm: challenge.logo_url_sm,
        status: challenge.status,
        skills: skills(challenge),
        interests: interests(challenge),
        geoloc: geoloc(challenge),
        is_owner: is_owner(challenge),
        is_admin: is_admin(challenge),
        is_member: is_member(challenge),
        users_sm: challenge.users_sm,
        program: [id: challenge.program.id, short_title: challenge.program.short_title, title: challenge.program.title, title_fr: challenge.program.title_fr],
        projects_count: challenge.projects_count,
        has_clapped: has_clapped(challenge),
        has_followed: has_followed(challenge),
        has_saved: has_saved(challenge),
        claps_count: challenge.claps_count,
        needs_count: challenge.needs_count,
        follower_count: challenge.follower_count,
        members_count: members_count(challenge)
      }
    end
  end
end
