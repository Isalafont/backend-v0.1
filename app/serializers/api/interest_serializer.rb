# frozen_string_literal: true

class Api::InterestSerializer < ActiveModel::Serializer
  attributes :id
end
