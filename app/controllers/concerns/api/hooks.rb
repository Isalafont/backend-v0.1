# frozen_string_literal: true

module Api::Hooks
  extend ActiveSupport::Concern

  included do
    before_action :hooks_authenticate_user!, only: %i[create_external_hook update_external_hook delete_external_hook get_external_hooks]
  end

  # the params for this controller look like this:
  # params
  #
  # {
  # hook: {
  #   hook_type: "Slack",
  #   hook_params: {
  #     hook_url: "URL",
  #     channel: "channel",
  #     username: "username"
  #   }
  # }
  # }

  def hook_new_member(hooks, user, obj)
    hooks.map do |hook|
      next unless hook.trigger_member

      SlackHookWorker.perform_async(hook.slackhooks.first.id, 'join', 'User', user.id, obj.class.name, obj.id) if hook.hook_type == 'Slack'
    end
  end

  def hook_new_post(hooks, post)
    hooks.map do |hook|
      next unless hook.trigger_post

      SlackHookWorker.perform_async(hook.slackhooks.first.id, 'new', 'Post', post.id) if hook.hook_type == 'Slack'
    end
  end

  def hook_new_need(hooks, need)
    hooks.map do |hook|
      next unless hook.trigger_need

      SlackHookWorker.perform_async(hook.slackhooks.first.id, 'new', 'Need', need.id) if hook.hook_type == 'Slack'
    end
  end

  def hook_new_project(hooks)
    hooks.map do |hook|
      next unless hook.trigger_project

      SlackHookWorker.perform_async(hook.slackhooks.first.id, message) if hook.hook_type == 'Slack'
    end
  end

  def hook_new_project_challenge(hooks, project)
    hooks.map do |hook|
      next unless hook.trigger_project

      SlackHookWorker.perform_async(hook.slackhooks.first.id, 'attach', 'Project', project.id) if hook.hook_type == 'Slack'
    end
  end

  def create_external_hook
    @hook = @obj.externalhooks.create(validate_hook)
    @hook.save!
    if validate_hook[:hook_type] == 'Slack'
      @hook.slackhooks.create(validate_hook_params[:hook_params])
      render json: { data: 'Hook created' }, status: :created
    else
      render json: { data: 'Hook Type not recognized' }, status: :unprocessable_entity
    end
  end

  def get_external_hooks
    render json: @obj.externalhooks, each_serializer: Api::HookSerializer
  end

  def update_external_hook
    hook_params = validate_hook
    @hook = Externalhook.find(params[:hook_id])
    old_hook = @hook.hook_type
    render json: { data: 'Something went wrong' }, status: :unprocessable_entity unless @hook.update_attributes(hook_params)
    # IF the hook type has changed
    if hook_params[:hook_type] != old_hook
      # First delete the previous hook
      @hook.slackhook.first.delete if old_hook == 'Slack'
      # Then recreate the new hook
      @hook.slackhooks.create(validate_hook_params[:hook_params]) if hook_params[:hook_type] == 'Slack'
      render json: { data: 'Hook updated' }, status: :ok
    else
      # We just update the attributes
      if hook_params[:hook_type] == 'Slack'
        slackhook = @hook.slackhooks.first
        slackhook.update_attributes(validate_hook_params[:hook_params])
        render json: { data: 'Hook updated' }, status: :ok
      else
        render json: { data: 'Hook Type not recognized' }, status: :unprocessable_entity
      end
    end
  end

  def delete_external_hook
    @hook = Externalhook.find(params[:hook_id])
    if @hook.delete
      render json: { data: 'Hook deleted' }, status: :ok
    else
      render json: { data: 'Something went wrong' }, status: :unprocessable_entity
    end
  end

  private

  def hooks_authenticate_user!
    authenticate_user!
  end

  def validate_hook
    params.require(:hook).permit(:hook_type, :trigger_need, :trigger_post, :trigger_member, :trigger_project)
  end

  def validate_hook_params
    params.require(:hook).permit(hook_params: {})
  end
end
