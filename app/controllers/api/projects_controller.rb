# frozen_string_literal: true

class Api::ProjectsController < ApplicationController
  # Before action validation are applied on different methods in order to
  # prevent events that are not destined to be executed, this also prevents
  # repeating the code on each method
  before_action :authenticate_user!, only: %i[create update destroy my_projects]
  before_action :find_project, except: %i[index create my_projects short_title_exist get_id_from_name recommended]
  before_action :set_obj, except: %i[index create destroy short_title_exist get_id_from_name recommended]
  before_action :is_admin, only: %i[update invite upload_banner remove_banner update_member remove_member
                                    get_custom_data create_custom_field
                                    update_custom_field delete_custom_field
                                    create_external_hook update_external_hook delete_external_hook get_external_hooks]
  before_action :is_member, only: %i[create_custom_data update_custom_data delete_custom_data get_my_custom_datas]

  include Api::Utils
  include Api::Follow
  include Api::Members
  include Api::Upload
  include Api::Relations
  include Api::Customfields
  include Api::Hooks
  include Api::ExternalLinks
  include Api::Recommendations

  # Before action for the relations Api::Relations
  # before_action :find_relation, only: [:follow, :clap, :save]

  def index
    # take order param in the url, and order array depending on its value
    param = if params[:order] == 'desc'
              'id DESC'
            else
              'id ASC'
            end
    @pagy, @projects = pagy(Project.where.not(status: 1).order(param).all)
    render json: @projects
  end

  def index_needs
    @pagy, @needs = pagy(@project.needs)
    render json: @needs, each_serializer: Api::NeedSerializer
  end

  def my_projects
    @projects = Project.with_role(:owner, current_user)
    @projects += Project.with_role(:admin, current_user)
    @projects += Project.with_role(:member, current_user)
    @pagy, @projects = pagy_array(@projects.uniq)
    render json: @projects
  end

  def create
    if !Project.where(short_title: params[:project][:short_title]).empty?
      render json: { data: 'ShortTitle is already taken' }, status: :unprocessable_entity
    else
      @project = Project.new(project_params)
      @project.draft!
      @project.creator_id = current_user.id
      @project.users << current_user
      if @project.save
        @project.update_skills(params[:project][:skills]) unless params[:project][:skills].nil?
        @project.update_interests(params[:project][:interests]) unless params[:project][:interests].nil?
        current_user.add_role :owner, @project
        current_user.add_role :admin, @project
        current_user.add_role :member, @project
        current_user.add_edge(@project, 'is_author_of')
        current_user.add_edge(@project, 'is_member_of')
        current_user.add_edge(@project, 'is_admin_of')
        current_user.add_edge(@project, 'is_owner_of')

        # @project.geocode('project')
        render json: { id: @project.id, data: 'Success' }, status: :created
      else
        render json: { data: 'Something went wrong :(' }, status: :unprocessable_entity
      end
    end
  end

  def show
    render json: @project, show_objects: true
  end

  def update
    if params[:project][:creator_id].present?
      if current_user.has_role? :owner, @project
        @project.roles.find_by(resource_type: @project.class.name, resource_id: @project.id, name: 'owner').delete
        @new_owner = User.find_by(id: params[:project][:creator_id])
        if @new_owner.nil?
          render json: { data: "New owner id:#{params[:project][:creator_id]} does not exist" }, status: :not_found
        else
          @new_owner.add_role :owner, @project
        end
      else
        render json: { data: 'You cannot change the creator_id' }, status: :forbidden
      end
    end
    if @project.update_attributes(project_params)
      @project.update_skills(params[:project][:skills]) unless params[:project][:skills].nil?
      @project.update_interests(params[:project][:interests]) unless params[:project][:interests].nil?
      render json: @project, status: :ok
    else
      render json: { data: 'Something went wrong :(' }, status: :unprocessable_entity
    end
  end

  def destroy
    # why count the users in a project?
    # this would be more logical @project.users.delete_all followed by status modification
    # Leo: Because if the project has only one user we consider it a "test" aka you can ACTUALLY deleted it
    # Else, it stays as archived
    if @project.users.count == 1
      @project.destroy
      render json: { data: "Project id:#{params[:id]} destroyed" }, status: :ok
    else
      # @project.update_attributes({status: "archived"})
      @project.archived!
      render json: { data: "Project id:#{@project.id} archived" }, status: :ok
    end
  end
  #
  # def upload_document
  #   documents = params[:documents]
  #   if !documents.nil?
  #     documents.each do |document|
  #       @project.documents.attach(document)
  #     end
  #     if !@project.documents.attached?
  #       render json: {data: "Something went wrong"}, status: :unprocessable_entity and return
  #     end
  #     render json: {data: "documents uploaded"}, status: :ok
  #   end
  # end

  private

  def find_project
    @project = Project.find(params[:id])
    render json: { data: 'Project not found' }, status: :not_found if @project.nil?
  end

  def set_obj
    @obj = @project
  end

  def project_params
    params.require(:project).permit(:title, :short_title, :banner_url, :logo_url, :description, :short_description,
                                    :country, :city, :address, :is_private, :creator_id, :stranger_id, :current_user_id,
                                    :desc_elevator_pitch, :desc_problem_statement, :desc_objectives, :desc_state_art, :desc_progress,
                                    :desc_stakeholder, :desc_impact_strat, :desc_ethical_statement, :desc_sustainability_scalability,
                                    :desc_communication_strat, :desc_funding, :desc_contributing)
  end
end
