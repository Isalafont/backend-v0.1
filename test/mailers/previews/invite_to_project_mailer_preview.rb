# frozen_string_literal: true

# Preview all emails at http://localhost:3000/rails/mailers/invite_to_project_mailer
class InviteToProjectMailerPreview < ActionMailer::Preview
end
