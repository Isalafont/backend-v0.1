# frozen_string_literal: true

# Preview all emails at http://localhost:3000/rails/mailers/invite_stranger_mailer
class InviteStrangerMailerPreview < ActionMailer::Preview
end
